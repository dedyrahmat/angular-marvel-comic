import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ComicService {

  constructor(private http: HttpClient) { }

  apiUrl = environment.apiURL;
  apiKey = environment.apiKey
  hash = environment.hash
  timestamp = environment.ts

  fetchListOfComics() {
    return this.http.get(`${this.apiUrl}/comics?ts=${this.timestamp}&apikey=${this.apiKey}&hash=${this.hash}&limit=100`);
  }
  fetchComicDetail(comicId) {
    return this.http.get(`${this.apiUrl}/comics/${comicId}?ts=${this.timestamp}&apikey=${this.apiKey}&hash=${this.hash}`);
  }
}
