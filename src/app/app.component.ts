import { Component, OnInit } from "@angular/core";
import { ComicService } from "./services/comic.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "marvel-comics";
  listComics: any = [];
  p: number = 1;
  filterParam: string
  showList: boolean = true
  showDetail: boolean = false
  detailComic: any = [];

  constructor(private comicServices: ComicService) {}

  ngOnInit(): void {
    this.fetchComicList()
  }

  fetchComicList(): void {
    this.listComics = []
    this.comicServices.fetchListOfComics().subscribe(
      data => {
        if(data["code"] == 200) {
          let results: [] = data["data"]["results"]
        results.sort(this.sortByAtoZ)
        this.listComics = results;
        }
      },
      error => {
        console.error(error);
      }
    );
  }
  sortByAtoZ(a, b) {
    if(a.title < b.title) { return -1; }
    if(a.title > b.title) { return 1; }
    return 0;
  }
  sortByZtoA(a, b) {
    if(a.title > b.title) { return -1; }
    if(a.title < b.title) { return 1; }
    return 0;
  }
  sortToRandomize(array) {
    let currentIndex = array.length, temporaryValue, randomIndex

    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex)
      currentIndex -= 1

      temporaryValue = array[currentIndex]
      array[currentIndex] = array[randomIndex]
      array[randomIndex] = temporaryValue
    }

    return array
  }

  applyFilter(event){
    this.p = 1
    switch (event) {
      case "AtoZ":
          this.listComics.sort(this.sortByAtoZ)
        break;

      case "ZtoA":
          this.listComics.sort(this.sortByZtoA)
        break;
      case "random":
        this.listComics = this.sortToRandomize(this.listComics)
        break;
      default:
          this.listComics.sort(this.sortByAtoZ)
        break;
    }
  }

  showComicDetail(comicId) {
    this.showList = false
    this.showDetail = true
    this.detailComic = []
    window.scroll(0, 0)
    console.log(comicId)

    this.comicServices.fetchComicDetail(comicId).subscribe((data) => {
      if(data["code"] == 200) {
        let results: [] = data["data"]["results"][0]
        // console.log(results)
        this.detailComic = results
      }
    }, (error) => {
      console.error(error)
    })
  }

  backToComicList() {
    this.showList = true
    this.showDetail = false
    this.fetchComicList()
  }
}
